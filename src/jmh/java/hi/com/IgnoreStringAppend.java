package hi.com;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.UUID;

@Warmup(iterations = 1, time = 1)
@Measurement(iterations = 1, time = 1)
@Fork(1)
@State(Scope.Thread)
public class IgnoreStringAppend {

  private String[] strings;

  @Setup
  public void setup() {
    strings = new String[1000];
    for (int i = 0; i < strings.length; i++) {
      strings[i] = UUID.randomUUID().toString();
    }
  }

  @Benchmark
  public void bmStringPlus(Blackhole bh) {
    bh.consume(strings[0] + strings[1]);
  }


}
