package tri.le.jmh;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Thread)
public class StringAppend {

  private String[] strings;

  @Setup
  public void setup() {
    strings = new String[1000];
    for (int i = 0; i < strings.length; i++) {
      strings[i] = UUID.randomUUID().toString();
    }
  }

  @Benchmark
  public void bmStringPlus(Blackhole bh) {
    bh.consume(strings[0] + strings[1]);
  }

  @Benchmark
  public void bmStringBuilder(Blackhole bh) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(strings[0]).append(strings[1]);
    bh.consume(stringBuilder.toString());
  }

  @Benchmark
  public void bmStringBuffer(Blackhole bh) {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(strings[0]).append(strings[1]);
    bh.consume(stringBuffer.toString());
  }

  @Benchmark
  public void bmStringJoin(Blackhole bh) {
    bh.consume(String.join("", strings[0], strings[1]));
  }

  @Benchmark
  public void bmStringConcat(Blackhole bh) {
    bh.consume(strings[0].concat(strings[1]));
  }

  @Benchmark
  public void bmStringPlusx5(Blackhole bh) {
    bh.consume(strings[0] + strings[1] + strings[2] + strings[3] + strings[4]);
  }

  @Benchmark
  public void bmStringBuilderx5(Blackhole bh) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(strings[0]).append(strings[1]).append(strings[2]).append(strings[3]).append(strings[4]);
    bh.consume(stringBuilder.toString());
  }

  @Benchmark
  public void bmStringBufferx5(Blackhole bh) {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(strings[0]).append(strings[1]).append(strings[2]).append(strings[3]).append(strings[4]);
    bh.consume(stringBuffer.toString());
  }

  @Benchmark
  public void bmStringJoinx5(Blackhole bh) {
    bh.consume(String.join("", strings[0], strings[1], strings[2], strings[3], strings[4]));
  }

  @Benchmark
  public void bmStringConcatx5(Blackhole bh) {
    bh.consume(strings[0].concat(strings[1]).concat(strings[2]).concat(strings[3]).concat(strings[4]));
  }

  @Benchmark
  public void bmStringPlusx1000(Blackhole bh) {
    String finalString = "";
    for (int i = 0; i < strings.length; i++) {
      finalString += strings[i];
    }
    bh.consume(finalString);
  }

  @Benchmark
  public void bmStringBuilderx1000(Blackhole bh) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < strings.length; i++) {
      stringBuilder.append(strings[i]);
    }
    bh.consume(stringBuilder.toString());
  }

  @Benchmark
  public void bmStringBufferx1000(Blackhole bh) {
    StringBuffer stringBuffer = new StringBuffer();
    for (int i = 0; i < strings.length; i++) {
      stringBuffer.append(strings[i]);
    }
    bh.consume(stringBuffer.toString());
  }

  @Benchmark
  public void bmStringJoinx1000(Blackhole bh) {
    bh.consume(String.join("", Arrays.asList(strings)));
  }

  @Benchmark
  public void bmStringConcatx1000(Blackhole bh) {
    String finalString = "";
    for (int i = 0; i < strings.length; i++) {
      finalString.concat(strings[i]);
    }
    bh.consume(finalString);
  }
}
